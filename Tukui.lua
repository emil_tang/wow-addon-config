
TukuiData = {
	["Ravencrest"] = {
		["Velann"] = {
			["Installation"] = {
				["Done"] = true,
			},
			["Misc"] = {
			},
			["Move"] = {
			},
			["ActionBars"] = {
				["HideBar5"] = true,
			},
			["DataTexts"] = {
				["Friends"] = {
					true, -- [1]
					3, -- [2]
				},
				["Character"] = {
					true, -- [1]
					2, -- [2]
				},
				["Guild"] = {
					true, -- [1]
					1, -- [2]
				},
				["Gold"] = {
					true, -- [1]
					6, -- [2]
				},
				["System"] = {
					true, -- [1]
					4, -- [2]
				},
				["Time"] = {
					true, -- [1]
					7, -- [2]
				},
				["MicroMenu"] = {
					true, -- [1]
					5, -- [2]
				},
			},
			["Chat"] = {
				["Frame3"] = {
					"TOPLEFT", -- [1]
					"TOPLEFT", -- [2]
					0, -- [3]
					0, -- [4]
					370, -- [5]
					108, -- [6]
				},
				["Frame1"] = {
					"BOTTOMLEFT", -- [1]
					"BOTTOMLEFT", -- [2]
					34, -- [3]
					50, -- [4]
					370, -- [5]
					108, -- [6]
				},
				["Frame2"] = {
					"TOPLEFT", -- [1]
					"TOPLEFT", -- [2]
					0, -- [3]
					0, -- [4]
					370, -- [5]
					108, -- [6]
				},
				["Frame4"] = {
					"BOTTOMRIGHT", -- [1]
					"BOTTOMRIGHT", -- [2]
					-34, -- [3]
					50, -- [4]
					370, -- [5]
					108, -- [6]
				},
			},
		},
		["Merii"] = {
			["Installation"] = {
				["Done"] = true,
			},
			["Misc"] = {
			},
			["Move"] = {
				["TukuiMicroMenu"] = {
					"LEFT", -- [1]
					"UIParent", -- [2]
					"LEFT", -- [3]
					376, -- [4]
					138.0000152587891, -- [5]
				},
				["TukuiTargetFrame"] = {
					"CENTER", -- [1]
					"UIParent", -- [2]
					"CENTER", -- [3]
					249.9999542236328, -- [4]
					-178.5000762939453, -- [5]
				},
				["VehicleSeatIndicator"] = {
					"LEFT", -- [1]
					"UIParent", -- [2]
					"LEFT", -- [3]
					16.00021362304688, -- [4]
					-8.999902725219727, -- [5]
				},
				["TukuiFocusFrame"] = {
					"CENTER", -- [1]
					"UIParent", -- [2]
					"CENTER", -- [3]
					338.00048828125, -- [4]
					107.0001831054688, -- [5]
				},
				["TukuiTargetTargetFrame"] = {
					"RIGHT", -- [1]
					"UIParent", -- [2]
					"RIGHT", -- [3]
					-439.9990539550781, -- [4]
					-191.0005493164063, -- [5]
				},
				["TukuiActionBar1"] = {
					"CENTER", -- [1]
					"UIParent", -- [2]
					"CENTER", -- [3]
					0.0005252140108495951, -- [4]
					-188.0004425048828, -- [5]
				},
				["TukuiExtraActionButton"] = {
					"BOTTOM", -- [1]
					"UIParent", -- [2]
					"BOTTOM", -- [3]
					0.0002626070054247975, -- [4]
					15.00017738342285, -- [5]
				},
				["TukuiPlayerCastBar"] = {
					"CENTER", -- [1]
					"UIParent", -- [2]
					"CENTER", -- [3]
					-6.565175135619938e-05, -- [4]
					-123.5000991821289, -- [5]
				},
				["TukuiBossFrame1"] = {
					"RIGHT", -- [1]
					"UIParent", -- [2]
					"RIGHT", -- [3]
					-314.9993286132813, -- [4]
					-139.9999542236328, -- [5]
				},
				["TukuiArenaFrame1"] = {
					"RIGHT", -- [1]
					"UIParent", -- [2]
					"RIGHT", -- [3]
					-314.9991760253906, -- [4]
					-139.9998321533203, -- [5]
				},
				["TukuiTotemBar"] = {
					"CENTER", -- [1]
					"UIParent", -- [2]
					"CENTER", -- [3]
					-199.9995727539063, -- [4]
					-250.9998626708984, -- [5]
				},
				["TukuiPlayerFrame"] = {
					"CENTER", -- [1]
					"UIParent", -- [2]
					"CENTER", -- [3]
					-249.9998168945313, -- [4]
					-178.50048828125, -- [5]
				},
			},
			["ActionBars"] = {
				["HideBar5"] = true,
			},
			["DataTexts"] = {
				["Friends"] = {
					true, -- [1]
					3, -- [2]
				},
				["Character"] = {
					true, -- [1]
					2, -- [2]
				},
				["Guild"] = {
					true, -- [1]
					1, -- [2]
				},
				["Gold"] = {
					true, -- [1]
					6, -- [2]
				},
				["MicroMenu"] = {
					true, -- [1]
					5, -- [2]
				},
				["Time"] = {
					true, -- [1]
					7, -- [2]
				},
				["System"] = {
					true, -- [1]
					4, -- [2]
				},
			},
			["Chat"] = {
				["Frame3"] = {
					"TOPLEFT", -- [1]
					"TOPLEFT", -- [2]
					0, -- [3]
					0, -- [4]
					370, -- [5]
					108, -- [6]
				},
				["Frame1"] = {
					"BOTTOMLEFT", -- [1]
					"BOTTOMLEFT", -- [2]
					34, -- [3]
					50, -- [4]
					370, -- [5]
					108, -- [6]
				},
				["Frame2"] = {
					"TOPLEFT", -- [1]
					"TOPLEFT", -- [2]
					0, -- [3]
					0, -- [4]
					370, -- [5]
					108, -- [6]
				},
				["Frame4"] = {
					"BOTTOMRIGHT", -- [1]
					"BOTTOMRIGHT", -- [2]
					-34, -- [3]
					50, -- [4]
					370, -- [5]
					108, -- [6]
				},
			},
		},
	},
}
TukuiSettings = {
}
TukuiGold = {
	["Ravencrest"] = {
		["Velann"] = 391492306,
		["Merii"] = 136875038,
	},
}
TukuiSettingsPerCharacter = {
	["Ravencrest"] = {
		["Velann"] = {
		},
		["Merii"] = {
			["Party"] = {
				["Enable"] = true,
				["ShowManaText"] = true,
			},
			["Auras"] = {
				["ClassicTimer"] = true,
			},
			["DataTexts"] = {
				["ClassColor"] = true,
			},
			["ActionBars"] = {
				["RightBar"] = true,
			},
			["Chat"] = {
				["TextFading"] = true,
			},
			["NamePlates"] = {
				["ColorThreat"] = true,
			},
			["UnitFrames"] = {
				["UnlinkCastBar"] = true,
			},
		},
	},
}
