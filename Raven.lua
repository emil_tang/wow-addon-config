
RavenDB = {
	["profileKeys"] = {
		["Velann - Ravencrest"] = "RealUI",
		["Merii - Ravencrest"] = "RealUI",
	},
	["global"] = {
		["Minimap"] = {
			["hide"] = true,
		},
		["FilterDebuff"] = {
			["TargetDebuffs"] = {
			},
		},
		["DefaultPoisonColor"] = {
			["a"] = 1,
			["g"] = 0.6313725490196078,
		},
		["Version"] = "7",
		["DefaultDiseaseColor"] = {
			["a"] = 1,
		},
		["DefaultCurseColor"] = {
			["a"] = 1,
			["r"] = 0.4196078431372549,
			["b"] = 0.6941176470588235,
		},
		["Settings"] = {
			["WeaponEnchants"] = {
				["showNoDuration"] = true,
				["pointYT"] = 0.4857143463059572,
				["pointW"] = 168.9999885429524,
				["pointH"] = 14.99997265772085,
				["pointX"] = 0.5,
				["pointXR"] = 0.3994047706772935,
				["pointY"] = 0.4999999657259233,
			},
			["Buffs"] = {
				["growDirection"] = false,
				["showNoDuration"] = true,
				["detectBuffsCastBy"] = "anyone",
				["timeOffset"] = 14,
				["spacingX"] = -9,
				["anchorX"] = -3,
				["pointYT"] = 0.001904704699695495,
				["reverseSort"] = true,
				["barWidth"] = 20,
				["i_iconSize"] = 30,
				["barHeight"] = 5,
				["i_spacingX"] = 3,
				["detectBuffs"] = true,
				["pointW"] = 37.00001862648837,
				["iconInset"] = -15,
				["configuration"] = 9,
				["hideLabel"] = true,
				["spacingY"] = -9,
				["checkDuration"] = true,
				["sor"] = "T",
				["iconSize"] = 37,
				["checkTimeLeft"] = true,
				["filterDuration"] = 60,
				["timeAlign"] = "LEFT",
				["i_spacingY"] = 15,
				["pointX"] = 0.9775,
				["iconAlign"] = "RIGHT",
				["filterTimeLeft"] = 60,
				["maxBars"] = 40,
				["timeInset"] = 16,
				["pointXR"] = 0.00119044043730982,
				["pointH"] = 37.00001862648837,
				["wrap"] = 20,
				["timeFormat"] = 23,
				["anchorY"] = -3,
				["pointY"] = 0.9638,
				["hideBar"] = true,
				["iconOffset"] = 8,
			},
		},
		["DefaultBuffColor"] = {
			["b"] = 0.2,
			["g"] = 0.796078431372549,
			["r"] = 0.5215686274509804,
		},
		["Defaults"] = {
			["barHeight"] = 18,
			["iconFSize"] = 14,
			["spacingY"] = 1,
			["bgAlpha"] = 0,
			["backdropInset"] = 4.01,
			["iconSize"] = 24,
			["hideClock"] = true,
			["borderOffset"] = 0,
			["timeFont"] = "Roboto Condensed",
			["labelFont"] = "Roboto",
			["barWidth"] = 158,
			["iconFont"] = "Roboto Condensed",
			["texture"] = "Plain",
			["hideSpark"] = true,
			["i_iconSize"] = 36,
			["borderWidth"] = 1,
			["i_barHeight"] = 1,
			["i_spacingY"] = -9,
			["i_spacingX"] = -9,
			["bgtexture"] = "Plain",
			["labelFSize"] = 12,
			["i_hideClock"] = true,
			["i_barWidth"] = 5,
			["timeFSize"] = 14,
			["iconOutline"] = false,
		},
		["DefaultCooldownColor"] = {
			["b"] = 0.2549019607843137,
			["g"] = 0.7803921568627451,
			["r"] = 0.8431372549019608,
		},
		["DefaultDebuffColor"] = {
			["b"] = 0,
			["g"] = 0,
			["r"] = 0.6470588235294118,
		},
		["DefaultMagicColor"] = {
			["a"] = 1,
			["r"] = 0.1450980392156863,
			["g"] = 0.4392156862745098,
			["b"] = 0.7176470588235294,
		},
		["SpellLists"] = {
			["PlayerInclusions"] = {
			},
			["ClassBuffs"] = {
			},
			["PlayerDebuffExclusions"] = {
			},
			["PlayerExclusions"] = {
			},
			["TargetExclusions"] = {
			},
		},
		["FilterBuff"] = {
			["PlayerBuffs"] = {
			},
			["PlayerBuffsExtra"] = {
			},
			["TargetBuffs"] = {
			},
		},
		["HighlightsEnabled"] = false,
		["userDefinedMessage"] = "User-defined function not valid",
		["SpellTypes"] = {
			[204213] = "Magic",
			[17] = "Magic",
			[21562] = "Magic",
			[14914] = "Magic",
			[65081] = "Magic",
			[589] = "Magic",
		},
		["ButtonFacade"] = true,
	},
	["profiles"] = {
		["RealUI"] = {
			["Durations"] = {
				[14914] = 7,
				["Moderate Insight"] = 15,
				["Jade Spirit"] = 12,
				[256374] = 12,
				["Blind"] = 60,
				["Food"] = 20,
				["Anticipation"] = 15,
				["Windswept Pages"] = 20,
				["Arcane Missiles!"] = 20,
				[194384] = 30,
				[345096] = 60,
				["Revealing Strike"] = 15,
				["Paralytic Poison"] = 15,
				["Leeching Poison"] = 3600.022,
				["Shallow Insight"] = 15,
				["Recently Bandaged"] = 60,
				[324748] = 10,
				["River's Song"] = 7,
				["Kidney Shot"] = 2,
				["Deserter"] = 900,
				["Enlightenment"] = 10,
				["First Aid"] = 8,
				["Adrenaline Rush"] = 20,
				["Tricks of the Trade"] = 6,
				["Cheap Shot"] = 4,
				["Vanish"] = 3,
				["Garrote"] = 18,
				["Earthbind"] = 5,
				["Slice and Dice"] = 27,
				["Rupture"] = 12,
				["Combat Readiness"] = 20,
				["Dancing Steel"] = 12,
				["Vendetta"] = 20,
				[336885] = 11.81,
				["Recuperate"] = 24,
				["Weakened Soul"] = 15,
				["Rejuvenation"] = 12.597,
				[77489] = 6,
				[208772] = 15,
				["Sprint"] = 8,
				[589] = 16,
				["Hand of Protection"] = 10,
				["Resurrection Sickness"] = 600,
				["Deep Insight"] = 15,
				["Frostbolt"] = 9,
				[331937] = 10,
				["Killing Spree"] = 2,
				["Blade Twisting"] = 8,
				[21562] = 3600.005,
				["Honorless Target"] = 30,
				["Hurricane"] = 12,
				["Cloak of Shadows"] = 5,
				[2479] = 30,
				["Forbearance"] = 60,
				["Harmony"] = 20,
				["Grace"] = 15,
				["Eye of Vengeance"] = 10,
				["Blindside"] = 10,
				[316859] = 15,
				["Evasion"] = 15,
				["Arrow of Time"] = 20,
				["Deadly Poison"] = 3600.022,
				["Plague of Ages"] = 9,
				[65081] = 3,
				[204213] = 20,
				["Power Word: Shield"] = 15,
				[6788] = 7.049,
				["Paralysis"] = 4,
				[17] = 15,
				["Drink"] = 20,
				[45242] = 8,
			},
			["muteSFX"] = true,
			["BarGroups"] = {
				["PlayerDebuffs"] = {
					["detectDebuffsCastBy"] = "anyone",
					["parentFrame"] = "oUF_RealUIPlayer_Overlay",
					["i_barHeight"] = 5,
					["name"] = "PlayerDebuffs",
					["timeSort"] = false,
					["i_iconSize"] = 30,
					["auto"] = true,
					["pointYT"] = 0.5027777180455772,
					["i_spacingX"] = 3,
					["pointW"] = 181.9999694824219,
					["labelInset"] = 3,
					["pointH"] = 18.00000381469727,
					["sor"] = "T",
					["filterDebuffTable"] = "PlayerDebuffExclusions",
					["filterCooldownTable"] = "PlayerExclusions",
					["bars"] = {
					},
					["timeAlign"] = "LEFT",
					["i_spacingY"] = 15,
					["pointX"] = 0.1635416825612386,
					["maxBars"] = 10,
					["pointXR"] = 0.7416666666666667,
					["configuration"] = 8,
					["wrap"] = 10,
					["timeFormat"] = 23,
					["showNoDuration"] = true,
					["filterBuffTable"] = "PlayerExclusions",
					["detectDebuffs"] = true,
					["filterDebuffSpells"] = true,
					["pointY"] = 0.4805556098718273,
					["i_barWidth"] = 20,
				},
				["TargetDebuffs"] = {
					["parentFrame"] = "oUF_RealUITarget_Overlay",
					["i_barHeight"] = 5,
					["name"] = "TargetDebuffs",
					["timeSort"] = false,
					["barColors"] = "Standard",
					["auto"] = true,
					["pointYT"] = 0.5027777180455772,
					["i_iconSize"] = 30,
					["i_spacingX"] = 3,
					["pointW"] = 182.0000457763672,
					["labelInset"] = 3,
					["pointH"] = 18.00000381469727,
					["sor"] = "T",
					["iconColors"] = "Normal",
					["filterDebuffTable"] = "ClassBuffs",
					["filterDebuffList"] = {
					},
					["filterCooldownTable"] = "ClassBuffs",
					["bars"] = {
					},
					["timeAlign"] = "RIGHT",
					["i_spacingY"] = 15,
					["i_barWidth"] = 20,
					["pointX"] = 0.7421874364217123,
					["desaturate"] = true,
					["detectDebuffsMonitor"] = "target",
					["maxBars"] = 8,
					["pointXR"] = 0.1630208730697632,
					["wrap"] = 10,
					["timeFormat"] = 23,
					["showNoDuration"] = true,
					["filterBuffTable"] = "ClassBuffs",
					["detectDebuffs"] = true,
					["filterDebuffSpells"] = true,
					["pointY"] = 0.4805556098718273,
				},
				["FocusBuffs"] = {
					["desaturateFriend"] = true,
					["detectDebuffsCastBy"] = "anyone",
					["hideSpark"] = false,
					["growDirection"] = false,
					["parentFrame"] = "oUF_RealUIFocus_Overlay",
					["detectBuffsMonitor"] = "focus",
					["name"] = "FocusBuffs",
					["timeOffset"] = 18,
					["timeSort"] = false,
					["spacingX"] = -9,
					["barHeight"] = 1,
					["auto"] = true,
					["pointYT"] = 0.5962962436021868,
					["i_barHeight"] = 5,
					["i_iconSize"] = 30,
					["i_spacingX"] = 3,
					["detectBuffs"] = true,
					["pointW"] = 36.00000762939453,
					["pointH"] = 36.00000762939453,
					["noTargetBuffs"] = false,
					["filterDebuffTypes"] = true,
					["hideLabel"] = true,
					["spacingY"] = -9,
					["sor"] = "T",
					["iconInset"] = -18,
					["iconSize"] = 36,
					["filterDebuffTable"] = "PlayerExclusions",
					["filterCooldownTable"] = "PlayerExclusions",
					["noPlayerBuffs"] = true,
					["bars"] = {
					},
					["barWidth"] = 5,
					["timeAlign"] = "LEFT",
					["i_spacingY"] = 15,
					["iconAlign"] = "RIGHT",
					["i_barWidth"] = 20,
					["pointX"] = 0.2588541825612386,
					["configuration"] = 9,
					["detectDebuffsMonitor"] = "focus",
					["maxBars"] = 7,
					["timeInset"] = 16,
					["pointXR"] = 0.7223958134651184,
					["wrap"] = 7,
					["timeFormat"] = 23,
					["filterBuffTable"] = "PlayerExclusions",
					["pointY"] = 0.3703704122326222,
					["desaturate"] = true,
					["hideBar"] = true,
					["iconOffset"] = 5,
				},
				["FocusDebuffs"] = {
					["detectDebuffsCastBy"] = "anyone",
					["noPlayerDebuffs"] = true,
					["hideSpark"] = false,
					["growDirection"] = false,
					["parentFrame"] = "oUF_RealUIFocus_Overlay",
					["name"] = "FocusDebuffs",
					["timeOffset"] = 18,
					["timeSort"] = false,
					["spacingX"] = -9,
					["auto"] = true,
					["pointYT"] = 0.6203703203973072,
					["i_barHeight"] = 5,
					["i_iconSize"] = 35,
					["i_spacingX"] = -8,
					["pointW"] = 36.00000762939453,
					["pointH"] = 36.00000762939453,
					["hideLabel"] = true,
					["spacingY"] = -9,
					["sor"] = "T",
					["iconSize"] = 36,
					["filterDebuffTable"] = "PlayerExclusions",
					["showNoDuration"] = true,
					["bars"] = {
					},
					["timeAlign"] = "LEFT",
					["i_spacingY"] = -8,
					["iconAlign"] = "RIGHT",
					["i_barWidth"] = 20,
					["pointX"] = 0.2588541825612386,
					["noTargetDebuffs"] = true,
					["detectDebuffsMonitor"] = "focus",
					["maxBars"] = 7,
					["timeInset"] = 16,
					["pointXR"] = 0.7223958134651184,
					["configuration"] = 9,
					["wrap"] = 7,
					["timeFormat"] = 23,
					["barWidth"] = 5,
					["filterBuffTable"] = "PlayerExclusions",
					["detectDebuffs"] = true,
					["iconInset"] = -18,
					["pointY"] = 0.3462963354375018,
					["barHeight"] = 1,
					["hideBar"] = true,
					["iconOffset"] = 5,
				},
				["PlayerBuffs"] = {
					["filterBuffSpells"] = true,
					["hideSpark"] = false,
					["growDirection"] = false,
					["iconInset"] = -18,
					["name"] = "PlayerBuffs",
					["timeOffset"] = 18,
					["timeSort"] = false,
					["spacingX"] = -9,
					["anchorX"] = -63,
					["minimumDuration"] = false,
					["filterBuffList"] = {
					},
					["auto"] = true,
					["pointYT"] = 0.5194444113209377,
					["i_barHeight"] = 5,
					["i_barWidth"] = 20,
					["i_iconSize"] = 25,
					["i_spacingX"] = 2,
					["detectBuffs"] = true,
					["pointW"] = 36.00000762939453,
					["barWidth"] = 5,
					["anchorPoint"] = "LEFT",
					["hideLabel"] = true,
					["configuration"] = 9,
					["spacingY"] = -9,
					["sor"] = "T",
					["iconSize"] = 36,
					["filterDebuffTable"] = "PlayerExclusions",
					["playerSort"] = true,
					["checkTimeLeft"] = true,
					["i_hideClock"] = false,
					["filterCooldownTable"] = "ClassBuffs",
					["filterDuration"] = 60,
					["bars"] = {
					},
					["timeAlign"] = "LEFT",
					["desaturate"] = true,
					["i_spacingY"] = 15,
					["iconAlign"] = "RIGHT",
					["pointX"] = 0.2395833492279053,
					["filterTimeLeft"] = 60,
					["maxBars"] = 12,
					["timeInset"] = 16,
					["pointXR"] = 0.7416666467984517,
					["minimumTimeLeft"] = false,
					["wrap"] = 6,
					["timeFormat"] = 23,
					["pointH"] = 36.00000762939453,
					["filterBuffTable"] = "PlayerExclusions",
					["detectBuffsCastBy"] = "anyone",
					["pointY"] = 0.4472222445138713,
					["barHeight"] = 1,
					["hideBar"] = true,
					["iconOffset"] = 5,
				},
				["Buffs"] = {
					["hideSpark"] = false,
					["growDirection"] = false,
					["name"] = "Buffs",
					["timeOffset"] = 18,
					["timeSort"] = false,
					["spacingX"] = -9,
					["auto"] = true,
					["reverseSort"] = true,
					["pointYT"] = 0.0009259189663265614,
					["i_barHeight"] = 5,
					["i_iconSize"] = 30,
					["i_spacingX"] = 3,
					["detectBuffs"] = true,
					["pointW"] = 36.00000762939453,
					["pointH"] = 36.00000762939453,
					["anchorPoint"] = "TOPRIGHT",
					["hideLabel"] = true,
					["spacingY"] = -9,
					["sor"] = "T",
					["iconSize"] = 36,
					["filterCooldownTable"] = "ClassBuffs",
					["filterDuration"] = 60,
					["bars"] = {
					},
					["barWidth"] = 5,
					["timeAlign"] = "LEFT",
					["i_spacingY"] = 15,
					["iconAlign"] = "RIGHT",
					["pointX"] = 0.9807290395100912,
					["configuration"] = 9,
					["filterTimeLeft"] = 60,
					["anchorFrame"] = "RealUIPositionersBuffs",
					["maxBars"] = 40,
					["timeInset"] = 16,
					["pointXR"] = 0.0005209565162658692,
					["wrap"] = 20,
					["timeFormat"] = 23,
					["filterDebuffTable"] = "ClassBuffs",
					["iconInset"] = -18,
					["showNoDuration"] = true,
					["filterBuffTable"] = "ClassBuffs",
					["detectBuffsCastBy"] = "anyone",
					["i_barWidth"] = 20,
					["pointY"] = 0.9657407368684825,
					["barHeight"] = 1,
					["hideBar"] = true,
					["iconOffset"] = 5,
				},
				["TargetBuffs"] = {
					["filterBuffSpells"] = true,
					["hideSpark"] = false,
					["parentFrame"] = "oUF_RealUITarget_Overlay",
					["iconInset"] = -18,
					["detectBuffsMonitor"] = "target",
					["name"] = "TargetBuffs",
					["timeOffset"] = 18,
					["timeSort"] = false,
					["spacingX"] = -9,
					["filterBuffList"] = {
					},
					["barHeight"] = 1,
					["filterBuffTypes"] = false,
					["auto"] = true,
					["pointYT"] = 0.5194444113209377,
					["detectBuffsCastBy"] = "anyone",
					["i_iconSize"] = 30,
					["i_spacingX"] = 3,
					["detectBuffs"] = true,
					["pointW"] = 36.00000762939453,
					["pointH"] = 36.00000762939453,
					["hideLabel"] = true,
					["configuration"] = 9,
					["spacingY"] = -9,
					["sor"] = "T",
					["iconSize"] = 36,
					["filterDebuffTable"] = "PlayerExclusions",
					["playerSort"] = true,
					["filterCooldownTable"] = "ClassBuffs",
					["showNoDuration"] = true,
					["bars"] = {
					},
					["barWidth"] = 5,
					["timeAlign"] = "LEFT",
					["i_spacingY"] = 15,
					["iconAlign"] = "RIGHT",
					["i_barWidth"] = 20,
					["pointX"] = 0.741666603088379,
					["desaturate"] = true,
					["maxBars"] = 12,
					["timeInset"] = 16,
					["pointXR"] = 0.2395833929379781,
					["desaturateFriend"] = true,
					["wrap"] = 6,
					["timeFormat"] = 23,
					["filterBuffTable"] = "ClassBuffs",
					["i_barHeight"] = 5,
					["pointY"] = 0.4472222445138713,
					["hideBar"] = true,
					["iconOffset"] = 5,
				},
				["ToTDebuffs"] = {
					["noPlayerDebuffs"] = true,
					["hideSpark"] = false,
					["parentFrame"] = "oUF_RealUITargetTarget_Overlay",
					["name"] = "ToTDebuffs",
					["timeOffset"] = 18,
					["timeSort"] = false,
					["spacingX"] = -9,
					["auto"] = true,
					["pointYT"] = 0.5962962436021868,
					["i_barHeight"] = 5,
					["i_iconSize"] = 35,
					["i_spacingX"] = -8,
					["pointW"] = 36.00000762939453,
					["pointH"] = 36.00000762939453,
					["noFocusDebuffs"] = false,
					["hideLabel"] = true,
					["spacingY"] = -9,
					["sor"] = "T",
					["iconSize"] = 36,
					["filterDebuffTable"] = "PlayerExclusions",
					["bars"] = {
					},
					["barWidth"] = 5,
					["timeAlign"] = "LEFT",
					["i_spacingY"] = -8,
					["iconAlign"] = "RIGHT",
					["i_barWidth"] = 20,
					["pointX"] = 0.7223957697550456,
					["configuration"] = 9,
					["detectDebuffsMonitor"] = "targettarget",
					["maxBars"] = 7,
					["timeInset"] = 16,
					["pointXR"] = 0.2588542262713114,
					["noTargetDebuffs"] = true,
					["wrap"] = 7,
					["timeFormat"] = 23,
					["showNoDuration"] = true,
					["filterBuffTable"] = "PlayerExclusions",
					["detectDebuffs"] = true,
					["iconInset"] = -18,
					["pointY"] = 0.3703704122326222,
					["barHeight"] = 1,
					["hideBar"] = true,
					["iconOffset"] = 5,
				},
			},
		},
		["Merii - Ravencrest"] = {
		},
	},
}
