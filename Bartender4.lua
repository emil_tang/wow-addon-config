
Bartender4DB = {
	["namespaces"] = {
		["StatusTrackingBar"] = {
			["profiles"] = {
				["Merii - Ravencrest"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Velann - Ravencrest"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
			},
		},
		["ActionBars"] = {
			["profiles"] = {
				["RealUI"] = {
					["actionbars"] = {
						{
							["version"] = 3,
							["position"] = {
								["y"] = -199.5,
								["x"] = -171.5,
								["point"] = "CENTER",
							},
							["flyoutDirection"] = "DOWN",
							["padding"] = -9,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[mod:ctrl][@focus,exists][harm,nodead][combat][group:party][group:raid][vehicleui]][overridebar][cursor]show;hide",
							},
							["hidemacrotext"] = true,
						}, -- [1]
						{
							["version"] = 3,
							["position"] = {
								["y"] = 89,
								["x"] = -171.5,
								["point"] = "BOTTOM",
							},
							["padding"] = -9,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl][@focus,exists][harm,nodead][combat][group:party][group:raid][vehicleui][cursor]show;hide",
							},
							["hidemacrotext"] = true,
						}, -- [2]
						{
							["version"] = 3,
							["position"] = {
								["y"] = 62,
								["x"] = -171.5,
								["point"] = "BOTTOM",
							},
							["padding"] = -9,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl][@focus,exists][harm,nodead][combat][group:party][group:raid][vehicleui][cursor]show;hide",
							},
							["hidemacrotext"] = true,
						}, -- [3]
						{
							["fadeoutalpha"] = 0,
							["padding"] = -9,
							["flyoutDirection"] = "LEFT",
							["position"] = {
								["y"] = 334.5,
								["x"] = -36,
								["point"] = "RIGHT",
							},
							["rows"] = 12,
							["hidemacrotext"] = true,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl][cursor]show;fade",
							},
							["version"] = 3,
						}, -- [4]
						{
							["fadeoutalpha"] = 0,
							["padding"] = -9,
							["flyoutDirection"] = "LEFT",
							["position"] = {
								["y"] = 10.5,
								["x"] = -36,
								["point"] = "RIGHT",
							},
							["rows"] = 12,
							["hidemacrotext"] = true,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl][cursor]show;fade",
							},
							["version"] = 3,
						}, -- [5]
						{
							["enabled"] = false,
						}, -- [6]
					},
				},
				["Merii - Ravencrest"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.4998474121094,
								["x"] = -231.5001831054688,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["RealUI-Healing"] = {
					["actionbars"] = {
						{
							["flyoutDirection"] = "DOWN",
							["version"] = 3,
							["position"] = {
								["y"] = -173.5,
								["x"] = -171.5,
								["point"] = "CENTER",
							},
							["padding"] = -9,
							["hidemacrotext"] = true,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[mod:ctrl][@focus,exists][harm,nodead][combat][group:party][group:raid][vehicleui]][overridebar][cursor]show;hide",
							},
						}, -- [1]
						{
							["padding"] = -9,
							["version"] = 3,
							["position"] = {
								["y"] = 87,
								["x"] = -171.5,
								["point"] = "BOTTOM",
							},
							["hidemacrotext"] = true,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl][@focus,exists][harm,nodead][combat][group:party][group:raid][vehicleui][cursor]show;hide",
							},
						}, -- [2]
						{
							["padding"] = -9,
							["version"] = 3,
							["position"] = {
								["y"] = 60,
								["x"] = -171.5,
								["point"] = "BOTTOM",
							},
							["hidemacrotext"] = true,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl][@focus,exists][harm,nodead][combat][group:party][group:raid][vehicleui][cursor]show;hide",
							},
						}, -- [3]
						{
							["flyoutDirection"] = "LEFT",
							["rows"] = 12,
							["version"] = 3,
							["fadeoutalpha"] = 0,
							["position"] = {
								["y"] = 334.5,
								["x"] = -36,
								["point"] = "RIGHT",
							},
							["padding"] = -9,
							["hidemacrotext"] = true,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl][cursor]show;fade",
							},
						}, -- [4]
						{
							["flyoutDirection"] = "LEFT",
							["rows"] = 12,
							["version"] = 3,
							["fadeoutalpha"] = 0,
							["position"] = {
								["y"] = 10.5,
								["x"] = -36,
								["point"] = "RIGHT",
							},
							["padding"] = -9,
							["hidemacrotext"] = true,
							["visibility"] = {
								["custom"] = true,
								["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl][cursor]show;fade",
							},
						}, -- [5]
						{
							["enabled"] = false,
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
				["Velann - Ravencrest"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.4998474121094,
								["x"] = -231.5001831054688,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
			},
		},
		["LibDualSpec-1.0"] = {
			["char"] = {
				["Velann - Ravencrest"] = {
					"RealUI-Healing", -- [1]
					"RealUI-Healing", -- [2]
					"RealUI", -- [3]
					["enabled"] = true,
				},
				["Merii - Ravencrest"] = {
					"RealUI", -- [1]
					"RealUI", -- [2]
					"RealUI-Healing", -- [3]
					["enabled"] = true,
				},
			},
		},
		["ExtraActionBar"] = {
			["profiles"] = {
				["RealUI"] = {
					["hideArtwork"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 120,
						["x"] = 126.5,
						["point"] = "BOTTOM",
						["scale"] = 0.985,
					},
				},
				["Merii - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -252.4999389648438,
						["x"] = -63.50006103515625,
						["point"] = "CENTER",
					},
				},
				["RealUI-Healing"] = {
					["hideArtwork"] = true,
					["position"] = {
						["y"] = 200,
						["x"] = 0,
						["point"] = "BOTTOM",
						["scale"] = 0.98,
					},
					["version"] = 3,
				},
				["Velann - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -252.4999389648438,
						["x"] = -63.50006103515625,
						["point"] = "CENTER",
					},
				},
			},
		},
		["MicroMenu"] = {
			["profiles"] = {
				["RealUI"] = {
					["enabled"] = false,
				},
				["Merii - Ravencrest"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["RealUI-Healing"] = {
					["enabled"] = false,
				},
				["Velann - Ravencrest"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
			},
		},
		["BagBar"] = {
			["profiles"] = {
				["RealUI"] = {
					["enabled"] = false,
				},
				["Merii - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["RealUI-Healing"] = {
					["enabled"] = false,
				},
				["Velann - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
			},
		},
		["BlizzardArt"] = {
			["profiles"] = {
				["Merii - Ravencrest"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Velann - Ravencrest"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
			},
		},
		["StanceBar"] = {
			["profiles"] = {
				["RealUI"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 49,
						["x"] = -157.5,
						["point"] = "BOTTOM",
						["scale"] = 1,
						["growHorizontal"] = "LEFT",
					},
					["padding"] = -7,
					["visibility"] = {
						["custom"] = true,
						["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl]show;fade",
					},
					["fadeoutalpha"] = 0,
				},
				["Merii - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -14.99996185302734,
						["x"] = -82.5,
						["point"] = "CENTER",
					},
				},
				["RealUI-Healing"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 49,
						["x"] = -157.5,
						["point"] = "BOTTOM",
						["scale"] = 1,
						["growHorizontal"] = "LEFT",
					},
					["fadeoutalpha"] = 0,
					["padding"] = -7,
					["visibility"] = {
						["custom"] = true,
						["customdata"] = "[petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl]show;fade",
					},
				},
				["Velann - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -14.99996185302734,
						["x"] = -82.5,
						["point"] = "CENTER",
					},
				},
			},
		},
		["PetBar"] = {
			["profiles"] = {
				["RealUI"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 124.5,
						["x"] = -8,
						["point"] = "LEFT",
					},
					["rows"] = 10,
					["padding"] = -7,
					["visibility"] = {
						["custom"] = true,
						["customdata"] = "[nopet][petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl]show;fade",
					},
					["fadeoutalpha"] = 0,
				},
				["Merii - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["RealUI-Healing"] = {
					["rows"] = 10,
					["version"] = 3,
					["position"] = {
						["y"] = 124.5,
						["x"] = -8,
						["point"] = "LEFT",
					},
					["padding"] = -7,
					["visibility"] = {
						["custom"] = true,
						["customdata"] = "[nopet][petbattle][overridebar][vehicleui][possessbar,@vehicle,exists]hide;[mod:ctrl]show;fade",
					},
					["fadeoutalpha"] = 0,
				},
				["Velann - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
			},
		},
		["Vehicle"] = {
			["profiles"] = {
				["RealUI"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -59.5,
						["x"] = -36,
						["point"] = "TOPRIGHT",
						["scale"] = 0.84,
					},
				},
				["Merii - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.5,
						["point"] = "CENTER",
					},
				},
				["RealUI-Healing"] = {
					["position"] = {
						["y"] = -59.5,
						["x"] = -36,
						["point"] = "TOPRIGHT",
						["scale"] = 0.84,
					},
					["version"] = 3,
				},
				["Velann - Ravencrest"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.5,
						["point"] = "CENTER",
					},
				},
			},
		},
	},
	["profileKeys"] = {
		["Velann - Ravencrest"] = "RealUI-Healing",
		["Merii - Ravencrest"] = "RealUI-Healing",
	},
	["profiles"] = {
		["RealUI"] = {
			["minimapIcon"] = {
				["hide"] = true,
			},
		},
		["Merii - Ravencrest"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["RealUI-Healing"] = {
			["minimapIcon"] = {
				["hide"] = true,
			},
		},
		["Velann - Ravencrest"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
	},
}
