
ProjectAzilrokaDB = {
	["profileKeys"] = {
		["Isrid - Ravencrest"] = "Default",
		["Velann - Ravencrest"] = "Default",
		["Gwaenni - Ravencrest"] = "Default",
		["Shyanne - Ravencrest"] = "Default",
		["Merii - Ravencrest"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["ReputationReward"] = {
				["ShowAll"] = true,
				["Enable"] = false,
			},
			["BigButtons"] = {
				["Enable"] = false,
			},
			["EnhancedFriendsList"] = {
				["Enable"] = false,
			},
			["DragonOverlay"] = {
				["Enable"] = false,
			},
			["MovableFrames"] = {
				["StaticPopup1"] = {
					["Points"] = {
						nil, -- [1]
						nil, -- [2]
						nil, -- [3]
						nil, -- [4]
						-100, -- [5]
					},
				},
				["Enable"] = false,
			},
			["OzCooldowns"] = {
				["Enable"] = false,
			},
			["QuestSounds"] = {
				["Enable"] = false,
			},
			["stAddonManager"] = {
				["Enable"] = false,
			},
			["MouseoverAuras"] = {
				["Enable"] = false,
			},
			["AuraReminder"] = {
				["Enable"] = false,
			},
			["Cooldown"] = {
				["Enable"] = false,
			},
			["SquareMinimapButtons"] = {
				["ButtonSpacing"] = 0,
			},
		},
	},
}
stAddonManagerProfilesDB = {
}
stAddonManagerServerDB = {
	["Ravencrest"] = {
		["Velann"] = true,
	},
}
