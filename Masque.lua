
MasqueDB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["profileKeys"] = {
		["Velann - Ravencrest"] = "RealUI",
		["Merii - Ravencrest"] = "RealUI",
	},
	["profiles"] = {
		["Default"] = {
			["Groups"] = {
				["Bartender4_4"] = {
					["Inherit"] = false,
				},
				["Bartender4_BagBar"] = {
					["Inherit"] = false,
				},
				["Bartender4_StanceBar"] = {
					["Inherit"] = false,
				},
				["ElvUI_ActionBars"] = {
					["Inherit"] = false,
				},
				["Bartender4_PetBar"] = {
					["Inherit"] = false,
				},
				["Bartender4_5"] = {
					["Inherit"] = false,
				},
				["Bartender4"] = {
					["Inherit"] = false,
				},
				["Bartender4_1"] = {
					["Inherit"] = false,
				},
				["ElvUI"] = {
					["Inherit"] = false,
				},
				["ElvUI_Buffs"] = {
					["Inherit"] = false,
				},
				["Bartender4_3"] = {
					["Inherit"] = false,
				},
				["Bartender4_2"] = {
					["Inherit"] = false,
				},
				["ElvUI_Stance Bar"] = {
					["Inherit"] = false,
				},
				["ElvUI_Debuffs"] = {
					["Inherit"] = false,
				},
				["ElvUI_Pet Bar"] = {
					["Inherit"] = false,
				},
				["Bartender4_6"] = {
					["Inherit"] = false,
				},
			},
		},
		["RealUI"] = {
			["Groups"] = {
				["Raven_FocusBuffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Raven_TargetBuffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_StanceBar"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Raven_PlayerBuffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_1"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["ElvUI_Buffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_3"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_2"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Raven_ToTDebuffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_BagBar"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_4"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Raven"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Masque"] = {
					["Backdrop"] = true,
					["Fonts"] = true,
					["SkinID"] = "RealUI",
				},
				["ElvUI_ActionBars"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Raven_Buffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_5"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["ElvUI_Debuffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Raven_TargetDebuffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["ElvUI"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_PetBar"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Raven_PlayerDebuffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["ElvUI_Stance Bar"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Raven_FocusDebuffs"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["ElvUI_Pet Bar"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
				["Bartender4_6"] = {
					["SkinID"] = "RealUI",
					["Backdrop"] = true,
					["Inherit"] = false,
				},
			},
		},
	},
}
