
SkadaDB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["hasUpgraded"] = true,
	["profileKeys"] = {
		["Merii - Ravencrest"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["modeclicks"] = {
				["Healing"] = 2,
				["Total healing"] = 1,
				["Damage"] = 3,
			},
			["windows"] = {
				{
					["barslocked"] = true,
					["y"] = 5.000030517578125,
					["x"] = 1438.000122070313,
					["title"] = {
						["textcolor"] = {
							["r"] = 1,
							["g"] = 1,
							["b"] = 1,
						},
						["color"] = {
							["r"] = 0.1254901960784314,
							["g"] = 0.1254901960784314,
							["b"] = 0.1294117647058823,
						},
						["font"] = "Expressway",
						["borderthickness"] = 0,
						["fontsize"] = 14,
						["texture"] = "ElvUI Blank",
					},
					["point"] = "TOPRIGHT",
					["barbgcolor"] = {
						["a"] = 0,
						["r"] = 0,
						["g"] = 0,
						["b"] = 0,
					},
					["barfontsize"] = 14,
					["mode"] = "Damage",
					["spark"] = false,
					["bartexture"] = "ElvUI Blank",
					["barwidth"] = 470,
					["background"] = {
						["strata"] = "LOW",
						["bordertexture"] = "None",
						["borderthickness"] = 0,
						["height"] = 188.0000152587891,
						["bordercolor"] = {
							["a"] = 0,
						},
						["texture"] = "None",
					},
					["barfont"] = "Expressway",
				}, -- [1]
			},
			["tooltiprows"] = 10,
			["versions"] = {
				["1.6.7"] = true,
				["1.8.0"] = true,
				["1.6.4"] = true,
				["1.6.3"] = true,
			},
			["setstokeep"] = 30,
			["tooltippos"] = "topleft",
			["reset"] = {
				["instance"] = 3,
				["join"] = 1,
			},
		},
	},
}
