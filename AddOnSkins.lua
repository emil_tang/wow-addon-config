
AddOnSkinsDB = {
	["profileKeys"] = {
		["Isrid - Ravencrest"] = "Default",
		["Velann - Ravencrest"] = "Default",
		["Gwaenni - Ravencrest"] = "Default",
		["Shyanne - Ravencrest"] = "Default",
		["Merii - Ravencrest"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["DBMFontSize"] = 15,
			["DBMSkinHalf"] = true,
			["DBMFont"] = "Expressway",
			["BackgroundTexture"] = "ElvUI Norm",
			["EmbedLeftWidth"] = 235,
			["EmbedSystem"] = true,
			["StatusBarTexture"] = "ElvUI",
			["EmbedBackdrop"] = false,
			["Bartender4"] = false,
		},
	},
}
AddOnSkinsDS = {
	[4.48] = {
	},
}
