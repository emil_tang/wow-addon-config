
BigWigs3DB = {
	["discord"] = 15,
	["namespaces"] = {
		["BigWigs_Plugins_Victory"] = {
		},
		["BigWigs_Plugins_Alt Power"] = {
		},
		["LibDualSpec-1.0"] = {
		},
		["BigWigs_Plugins_Sounds"] = {
		},
		["BigWigs_Plugins_AutoReply"] = {
		},
		["BigWigs_Plugins_Countdown"] = {
		},
		["BigWigs_Plugins_AltPower"] = {
		},
		["BigWigs_Plugins_Colors"] = {
		},
		["BigWigs_Plugins_Raid Icons"] = {
		},
		["BigWigs_Plugins_InfoBox"] = {
			["profiles"] = {
				["Default"] = {
					["posx"] = 327.8221867574575,
					["posy"] = 450.1333008302572,
				},
			},
		},
		["BigWigs_Plugins_Bars"] = {
			["profiles"] = {
				["Default"] = {
					["barStyle"] = "ElvUI",
					["BigWigsEmphasizeAnchor_width"] = 220.0000610351563,
					["BigWigsEmphasizeAnchor_height"] = 22.00003242492676,
					["fontName"] = "Expressway",
					["BigWigsAnchor_width"] = 175,
					["BigWigsAnchor_y"] = 336,
					["BigWigsEmphasizeAnchor_x"] = 822.0888219897097,
					["BigWigsAnchor_height"] = 20.0000171661377,
					["font"] = "Fonts\\FRIZQT__.TTF",
					["emphasizeGrowup"] = true,
					["BigWigsAnchor_x"] = 16,
					["spacing"] = 4,
					["BigWigsEmphasizeAnchor_y"] = 323.6890269351534,
					["growup"] = true,
					["emphasizeMultiplier"] = 1,
					["texture"] = "ElvUI Blank",
				},
			},
		},
		["BigWigs_Plugins_Super Emphasize"] = {
		},
		["BigWigs_Plugins_BossBlock"] = {
		},
		["BigWigs_Plugins_Wipe"] = {
		},
		["BigWigs_Plugins_Messages"] = {
			["profiles"] = {
				["Default"] = {
					["font"] = "Fonts\\FRIZQT__.TTF",
					["fontSize"] = 18,
				},
			},
		},
		["BigWigs_Plugins_Proximity"] = {
			["profiles"] = {
				["Default"] = {
					["posx"] = 1024,
					["fontSize"] = 18,
					["height"] = 120.0000076293945,
					["font"] = "Fonts\\FRIZQT__.TTF",
					["posy"] = 346,
					["width"] = 139.9999847412109,
				},
			},
		},
		["BigWigs_Plugins_Pull"] = {
		},
		["BigWigs_Plugins_Statistics"] = {
		},
	},
	["wipe80"] = true,
	["profileKeys"] = {
		["Velann - Ravencrest"] = "Default",
		["Merii - Ravencrest"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["fakeDBMVersion"] = true,
		},
	},
}
BigWigsIconDB = {
}
BigWigsStatsDB = {
	[2296] = {
		[2424] = {
			["heroic"] = {
				["wipes"] = 33,
			},
		},
		[2425] = {
			["heroic"] = {
				["kills"] = 2,
				["wipes"] = 13,
				["best"] = 495.7219999999998,
			},
		},
		[2418] = {
			["heroic"] = {
				["kills"] = 2,
				["best"] = 325.5,
				["wipes"] = 4,
			},
		},
		[2426] = {
			["heroic"] = {
				["best"] = 407.1770000000252,
				["kills"] = 1,
			},
		},
		[2420] = {
			["heroic"] = {
				["kills"] = 1,
				["wipes"] = 10,
				["best"] = 282.8020000000252,
			},
		},
		[2428] = {
			["heroic"] = {
				["kills"] = 2,
				["best"] = 252.2200000000012,
				["wipes"] = 1,
			},
		},
		[2429] = {
			["heroic"] = {
				["best"] = 276.4919999999984,
				["kills"] = 2,
			},
		},
		[2422] = {
			["heroic"] = {
				["best"] = 289.5339999999851,
				["kills"] = 2,
			},
		},
		[2393] = {
			["heroic"] = {
				["best"] = 242.2069999999949,
				["kills"] = 3,
			},
		},
		[2394] = {
			["heroic"] = {
				["kills"] = 2,
				["wipes"] = 2,
				["best"] = 331.2789999999804,
			},
		},
	},
}
