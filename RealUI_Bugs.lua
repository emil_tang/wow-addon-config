
RealUI_Storage = {
	["nibRealUI"] = {
		["nibRealUIDB"] = {
			["char"] = {
				["Velann - Ravencrest"] = {
					["layout"] = {
						["current"] = 2,
					},
					["init"] = {
						["installStage"] = -1,
						["needchatmoved"] = false,
						["initialized"] = true,
					},
				},
				["Merii - Ravencrest"] = {
					["init"] = {
						["installStage"] = -1,
						["needchatmoved"] = false,
						["initialized"] = true,
					},
					["layout"] = {
						["current"] = 2,
					},
				},
			},
			["profileKeys"] = {
				["Velann - Ravencrest"] = "RealUI",
				["Merii - Ravencrest"] = "RealUI",
			},
			["namespaces"] = {
				["ScreenSaver"] = {
				},
				["ActionBars"] = {
				},
				["LibDualSpec-1.0"] = {
					["char"] = {
						["Velann - Ravencrest"] = {
							"RealUI-Healing", -- [1]
							"RealUI-Healing", -- [2]
						},
						["Merii - Ravencrest"] = {
							[3] = "RealUI-Healing",
						},
					},
				},
				["AddonListAdv"] = {
				},
				["AddonControl"] = {
				},
				["DragEmAll"] = {
					["global"] = {
						["AddonList"] = {
							["y"] = 24.00009155273438,
							["x"] = -6.103515625e-05,
							["point"] = "CENTER",
							["seen"] = true,
							["scale"] = 1,
						},
						["GameMenuFrame"] = {
							["y"] = 4.57763671875e-05,
							["point"] = "CENTER",
							["seen"] = true,
							["scale"] = 1,
						},
						["PlayerTalentFrame"] = {
							["y"] = -116,
							["x"] = 16,
							["seen"] = true,
							["scale"] = 1,
						},
						["CalendarFrame"] = {
							["y"] = -95.9998779296875,
							["x"] = 16,
							["seen"] = true,
							["scale"] = 1,
						},
						["WorldMapFrame"] = {
							["x"] = 16,
							["scale"] = 1,
							["seen"] = true,
						},
					},
				},
				["Loot"] = {
					["profiles"] = {
						["RealUI"] = {
							["loot"] = {
							},
						},
					},
				},
				["MinimapAdv"] = {
					["profiles"] = {
						["RealUI"] = {
							["information"] = {
								["location"] = true,
							},
						},
					},
				},
				["ClassResource"] = {
				},
				["CooldownCount"] = {
				},
				["Positioners"] = {
				},
				["InterfaceTweaks"] = {
					["global"] = {
						["dragFrames"] = true,
						["bindings"] = true,
						["mouseTrail"] = true,
					},
				},
				["Objectives Adv."] = {
				},
				["CastBars"] = {
				},
				["CombatFader"] = {
				},
				["FrameMover"] = {
					["profiles"] = {
						["RealUI"] = {
							["uiframes"] = {
								["zonetext"] = {
								},
								["errorframe"] = {
								},
								["playerpowerbaralt"] = {
								},
								["vsi"] = {
								},
							},
						},
					},
				},
				["AltPowerBar"] = {
				},
				["EventNotifier"] = {
				},
				["MirrorBar"] = {
				},
				["Infobar"] = {
					["char"] = {
						["Velann - Ravencrest"] = {
							["progressState"] = "rep",
						},
					},
					["profiles"] = {
						["RealUI"] = {
							["blocks"] = {
								["realui"] = {
									["durability"] = {
									},
									["guild"] = {
									},
									["progress"] = {
									},
									["bags"] = {
									},
									["friends"] = {
									},
									["currency"] = {
									},
									["netstats"] = {
									},
									["spec"] = {
									},
								},
							},
						},
					},
				},
				["UnitFrames"] = {
				},
				["Chat"] = {
					["profiles"] = {
						["RealUI"] = {
							["modules"] = {
								["history"] = {
								},
								["tabs"] = {
								},
							},
						},
					},
				},
			},
			["global"] = {
				["currency"] = {
					["Ravencrest"] = {
						["Alliance"] = {
							["Velann"] = {
								[1602] = 475,
								[1816] = 486,
								[1820] = 80,
								["lastSeen"] = 1610306864,
								[1828] = 280,
								["class"] = "PRIEST",
								[1885] = 21,
								[1813] = 68,
								["token3"] = 1828,
								[1792] = 11425,
								[1166] = 170,
								["money"] = 391492306,
								["token2"] = 1792,
								["token1"] = 1767,
								[1767] = 718,
							},
							["Merii"] = {
								["lastSeen"] = 1610302660,
								["money"] = 141715364,
								["class"] = "SHAMAN",
								[1754] = 2,
								[1755] = 51132,
								[1717] = 9,
								[1166] = 1005,
								[1560] = 922,
								[1710] = 9,
								["token1"] = 1754,
								[1704] = 9,
							},
						},
					},
				},
				["tags"] = {
					["firsttime"] = false,
				},
				["verinfo"] = {
					2, -- [1]
					2, -- [2]
					6, -- [3]
					["string"] = "2.2.6",
				},
			},
			["profiles"] = {
				["RealUI"] = {
					["registeredChars"] = {
						["Velann - Ravencrest"] = true,
						["Merii - Ravencrest"] = true,
					},
					["positions"] = {
						{
							["ActionBarsBotY"] = 23,
						}, -- [1]
						{
							["ActionBarsBotY"] = 23,
						}, -- [2]
					},
				},
				["RealUI-Healing"] = {
				},
			},
		},
	},
}
RealUI_Debug = {
}
