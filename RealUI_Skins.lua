
RealUI_SkinsDB = {
	["profileKeys"] = {
		["Velann - Ravencrest"] = "Default",
		["Merii - Ravencrest"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["frameColor"] = {
				["b"] = 0,
				["g"] = 0,
				["r"] = 0,
			},
			["customScale"] = 0.7111111111111111,
			["fonts"] = {
				["normal"] = {
					["name"] = "Roboto Condensed Bold",
					["path"] = "Interface\\AddOns\\Kui_Media\\f\\roboto.ttf",
				},
				["header"] = {
					["name"] = "Roboto Condensed Bold",
					["path"] = "Interface\\AddOns\\Kui_Media\\f\\roboto.ttf",
				},
				["chat"] = {
					["name"] = "Roboto Condensed Bold",
					["path"] = "Interface\\AddOns\\Kui_Media\\f\\roboto.ttf",
				},
			},
			["classColors"] = {
				["DEATHKNIGHT"] = {
					["r"] = 0.7686257362365723,
					["colorStr"] = "ffc31d39",
					["g"] = 0.117646798491478,
					["b"] = 0.2274504750967026,
				},
				["WARRIOR"] = {
					["r"] = 0.7764688730239868,
					["colorStr"] = "ffc59a6c",
					["g"] = 0.6078417897224426,
					["b"] = 0.427450031042099,
				},
				["SHAMAN"] = {
					["r"] = 0,
					["colorStr"] = "ff006fdc",
					["g"] = 0.4392147064208984,
					["b"] = 0.8666647672653198,
				},
				["MAGE"] = {
					["r"] = 0.2470582872629166,
					["colorStr"] = "ff3ec6ea",
					["g"] = 0.7803904414176941,
					["b"] = 0.9215666055679321,
				},
				["PRIEST"] = {
					["r"] = 0.9999977946281433,
					["colorStr"] = "fffefefe",
					["g"] = 0.9999977946281433,
					["b"] = 0.9999977946281433,
				},
				["PALADIN"] = {
					["r"] = 0.9568606615066528,
					["colorStr"] = "fff38bb9",
					["g"] = 0.549018383026123,
					["b"] = 0.7294101715087891,
				},
				["WARLOCK"] = {
					["r"] = 0.5294106006622314,
					["colorStr"] = "ff8687ed",
					["g"] = 0.5333321690559387,
					["b"] = 0.933331310749054,
				},
				["DEMONHUNTER"] = {
					["r"] = 0.639214277267456,
					["colorStr"] = "ffa22fc8",
					["g"] = 0.188234880566597,
					["b"] = 0.7882335782051086,
				},
				["ROGUE"] = {
					["r"] = 0.9999977946281433,
					["colorStr"] = "fffef367",
					["g"] = 0.9568606615066528,
					["b"] = 0.4078422486782074,
				},
				["DRUID"] = {
					["r"] = 0.9999977946281433,
					["colorStr"] = "fffe7b09",
					["g"] = 0.4862734377384186,
					["b"] = 0.03921560198068619,
				},
				["MONK"] = {
					["r"] = 0,
					["colorStr"] = "ff00fe97",
					["g"] = 0.9999977946281433,
					["b"] = 0.5960771441459656,
				},
				["HUNTER"] = {
					["r"] = 0.6666651964187622,
					["colorStr"] = "ffa9d271",
					["g"] = 0.8274491429328918,
					["b"] = 0.447057843208313,
				},
			},
			["buttonColor"] = {
				["b"] = 0.25,
				["g"] = 0.25,
				["r"] = 0.25,
			},
		},
	},
}
