
BetterWardrobe_Options = {
	["profileKeys"] = {
		["Merii - Ravencrest"] = "Default",
		["Shyanne - Ravencrest"] = "Default",
		["Isrid - Ravencrest"] = "Default",
		["Gwaenni - Ravencrest"] = "Default",
		["Velann - Ravencrest"] = "Default",
		["Mareen - Ravencrest"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
		},
	},
}
BetterWardrobe_CharacterData = {
	["profileKeys"] = {
		["Merii - Ravencrest"] = "Merii - Ravencrest",
		["Shyanne - Ravencrest"] = "Shyanne - Ravencrest",
		["Isrid - Ravencrest"] = "Isrid - Ravencrest",
		["Gwaenni - Ravencrest"] = "Gwaenni - Ravencrest",
		["Velann - Ravencrest"] = "Velann - Ravencrest",
		["Mareen - Ravencrest"] = "Mareen - Ravencrest",
	},
	["profiles"] = {
		["Merii - Ravencrest"] = {
			["lastTransmogOutfitIDSpec"] = {
				"0", -- [1]
				[3] = "0",
			},
			["listUpdate"] = 1,
		},
		["Velann - Ravencrest"] = {
			["listUpdate"] = 1,
			["lastTransmogOutfitIDSpec"] = {
				[3] = "",
				[2] = "",
			},
		},
		["Shyanne - Ravencrest"] = {
			["listUpdate"] = 1,
		},
		["Mareen - Ravencrest"] = {
			["listUpdate"] = 1,
		},
	},
}
BetterWardrobe_SavedSetData = {
	["global"] = {
		["sets"] = {
			["Merii - Ravencrest"] = {
				{
					["isClass"] = true,
					["type"] = "Saved",
					["description"] = "",
					["label"] = "Saved Set",
					["icon"] = 132647,
					["limitedTimeSet"] = false,
					["uiOrder"] = 100,
					["hiddenUtilCollected"] = false,
					["expansionID"] = 1,
					["sources"] = {
						77344, -- [1]
						0, -- [2]
						10659, -- [3]
						20496, -- [4]
						9514, -- [5]
						94113, -- [6]
						9427, -- [7]
						94140, -- [8]
						108790, -- [9]
						94109, -- [10]
						0, -- [11]
						0, -- [12]
						0, -- [13]
						0, -- [14]
						77345, -- [15]
						10333, -- [16]
						0, -- [17]
						0, -- [18]
						0, -- [19]
					},
					["name"] = "e",
					["setID"] = 5000,
					["patchID"] = "",
					["collected"] = true,
					["favorite"] = false,
					["items"] = {
					},
				}, -- [1]
			},
			["Shyanne - Ravencrest"] = {
			},
			["Gwaenni - Ravencrest"] = {
			},
			["Isrid - Ravencrest"] = {
			},
			["Velann - Ravencrest"] = {
				{
					["isClass"] = true,
					["type"] = "Saved",
					["description"] = "",
					["label"] = "Saved Set",
					["icon"] = 132704,
					["limitedTimeSet"] = false,
					["uiOrder"] = 100,
					["hiddenUtilCollected"] = false,
					["expansionID"] = 1,
					["sources"] = {
						15014, -- [1]
						0, -- [2]
						12199, -- [3]
						878, -- [4]
						20209, -- [5]
						20227, -- [6]
						20213, -- [7]
						20230, -- [8]
						20236, -- [9]
						20221, -- [10]
						0, -- [11]
						0, -- [12]
						0, -- [13]
						0, -- [14]
						77345, -- [15]
						114134, -- [16]
						111381, -- [17]
						0, -- [18]
						83203, -- [19]
					},
					["name"] = "belf",
					["setID"] = 5000,
					["patchID"] = "",
					["collected"] = true,
					["favorite"] = false,
					["items"] = {
					},
				}, -- [1]
				{
					["isClass"] = true,
					["type"] = "Saved",
					["description"] = "",
					["label"] = "Saved Set",
					["icon"] = 132704,
					["limitedTimeSet"] = false,
					["uiOrder"] = 200,
					["hiddenUtilCollected"] = false,
					["expansionID"] = 1,
					["sources"] = {
						12292, -- [1]
						0, -- [2]
						12199, -- [3]
						0, -- [4]
						20209, -- [5]
						20227, -- [6]
						20213, -- [7]
						20230, -- [8]
						20236, -- [9]
						20221, -- [10]
						0, -- [11]
						0, -- [12]
						0, -- [13]
						0, -- [14]
						24733, -- [15]
						13943, -- [16]
						15772, -- [17]
						0, -- [18]
						83203, -- [19]
					},
					["name"] = "belf2",
					["setID"] = 5001,
					["patchID"] = "",
					["collected"] = true,
					["favorite"] = false,
					["items"] = {
					},
				}, -- [2]
				{
					["isClass"] = true,
					["type"] = "Saved",
					["description"] = "",
					["label"] = "Saved Set",
					["icon"] = 132704,
					["limitedTimeSet"] = false,
					["uiOrder"] = 300,
					["hiddenUtilCollected"] = false,
					["expansionID"] = 1,
					["sources"] = {
						12292, -- [1]
						0, -- [2]
						12199, -- [3]
						0, -- [4]
						20209, -- [5]
						20227, -- [6]
						20213, -- [7]
						20230, -- [8]
						20236, -- [9]
						20221, -- [10]
						0, -- [11]
						0, -- [12]
						0, -- [13]
						0, -- [14]
						14911, -- [15]
						42902, -- [16]
						0, -- [17]
						0, -- [18]
						83203, -- [19]
					},
					["name"] = "venthyr",
					["setID"] = 5002,
					["patchID"] = "",
					["collected"] = true,
					["favorite"] = false,
					["items"] = {
					},
				}, -- [3]
				{
					["isClass"] = true,
					["type"] = "Saved",
					["description"] = "",
					["label"] = "Saved Set",
					["icon"] = 132704,
					["limitedTimeSet"] = false,
					["uiOrder"] = 400,
					["hiddenUtilCollected"] = false,
					["expansionID"] = 1,
					["sources"] = {
						12292, -- [1]
						0, -- [2]
						12199, -- [3]
						83202, -- [4]
						20209, -- [5]
						20227, -- [6]
						20213, -- [7]
						20230, -- [8]
						20236, -- [9]
						20221, -- [10]
						0, -- [11]
						0, -- [12]
						0, -- [13]
						0, -- [14]
						24733, -- [15]
						19438, -- [16]
						0, -- [17]
						0, -- [18]
						83203, -- [19]
					},
					["name"] = "whatever",
					["setID"] = 5003,
					["patchID"] = "",
					["collected"] = true,
					["favorite"] = false,
					["items"] = {
					},
				}, -- [4]
			},
		},
	},
	["profileKeys"] = {
		["Merii - Ravencrest"] = "Merii - Ravencrest",
		["Shyanne - Ravencrest"] = "Shyanne - Ravencrest",
		["Isrid - Ravencrest"] = "Isrid - Ravencrest",
		["Gwaenni - Ravencrest"] = "Gwaenni - Ravencrest",
		["Velann - Ravencrest"] = "Velann - Ravencrest",
		["Mareen - Ravencrest"] = "Mareen - Ravencrest",
	},
	["profiles"] = {
		["Gwaenni - Ravencrest"] = {
		},
		["Shyanne - Ravencrest"] = {
		},
		["Velann - Ravencrest"] = {
		},
	},
}
BetterWardrobe_SubstituteItemData = {
	["profileKeys"] = {
		["Merii - Ravencrest"] = "Default",
		["Shyanne - Ravencrest"] = "Default",
		["Isrid - Ravencrest"] = "Default",
		["Gwaenni - Ravencrest"] = "Default",
		["Velann - Ravencrest"] = "Default",
		["Mareen - Ravencrest"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
		},
	},
}
BetterWardrobe_ListData = {
	["favoritesDB"] = {
		["profileKeys"] = {
			["Merii - Ravencrest"] = "Merii - Ravencrest",
			["Velann - Ravencrest"] = "Velann - Ravencrest",
			["Isrid - Ravencrest"] = "Isrid - Ravencrest",
			["Gwaenni - Ravencrest"] = "Gwaenni - Ravencrest",
			["Shyanne - Ravencrest"] = "Shyanne - Ravencrest",
			["Mareen - Ravencrest"] = "Mareen - Ravencrest",
		},
		["profiles"] = {
			["Merii - Ravencrest"] = {
			},
			["Shyanne - Ravencrest"] = {
			},
			["Isrid - Ravencrest"] = {
			},
			["Gwaenni - Ravencrest"] = {
			},
			["Velann - Ravencrest"] = {
			},
			["Mareen - Ravencrest"] = {
			},
		},
	},
	["collectionListDB"] = {
		["profileKeys"] = {
			["Merii - Ravencrest"] = "Merii - Ravencrest",
			["Velann - Ravencrest"] = "Velann - Ravencrest",
			["Isrid - Ravencrest"] = "Isrid - Ravencrest",
			["Gwaenni - Ravencrest"] = "Gwaenni - Ravencrest",
			["Shyanne - Ravencrest"] = "Shyanne - Ravencrest",
			["Mareen - Ravencrest"] = "Mareen - Ravencrest",
		},
		["profiles"] = {
			["Merii - Ravencrest"] = {
				["lists"] = {
					{
						["name"] = "Collection List",
					}, -- [1]
				},
			},
			["Velann - Ravencrest"] = {
				["lists"] = {
					{
						["name"] = "Collection List",
					}, -- [1]
				},
			},
			["Isrid - Ravencrest"] = {
			},
			["Gwaenni - Ravencrest"] = {
			},
			["Shyanne - Ravencrest"] = {
				["lists"] = {
					{
						["name"] = "Collection List",
					}, -- [1]
				},
			},
			["Mareen - Ravencrest"] = {
				["lists"] = {
					{
						["set"] = {
						},
						["item"] = {
						},
						["name"] = "Collection List",
						["extraset"] = {
						},
					}, -- [1]
				},
			},
		},
	},
	["lastUpdte"] = 1,
	["OutfitDB"] = {
		["profileKeys"] = {
			["Merii - Ravencrest"] = "Merii - Ravencrest",
			["Velann - Ravencrest"] = "Velann - Ravencrest",
			["Isrid - Ravencrest"] = "Isrid - Ravencrest",
			["Gwaenni - Ravencrest"] = "Gwaenni - Ravencrest",
			["Shyanne - Ravencrest"] = "Shyanne - Ravencrest",
			["Mareen - Ravencrest"] = "Mareen - Ravencrest",
		},
		["char"] = {
			["Merii - Ravencrest"] = {
				["lastTransmogOutfitIDSpec"] = {
					"0", -- [1]
					[3] = "0",
				},
			},
			["Gwaenni - Ravencrest"] = {
				["lastTransmogOutfitIDSpec"] = {
					"", -- [1]
				},
			},
			["Velann - Ravencrest"] = {
				["lastTransmogOutfitIDSpec"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
			},
			["Isrid - Ravencrest"] = {
				["lastTransmogOutfitIDSpec"] = {
					[3] = "",
				},
			},
		},
	},
	["HiddenAppearanceDB"] = {
		["profileKeys"] = {
			["Merii - Ravencrest"] = "Merii - Ravencrest",
			["Velann - Ravencrest"] = "Velann - Ravencrest",
			["Isrid - Ravencrest"] = "Isrid - Ravencrest",
			["Gwaenni - Ravencrest"] = "Gwaenni - Ravencrest",
			["Shyanne - Ravencrest"] = "Shyanne - Ravencrest",
			["Mareen - Ravencrest"] = "Mareen - Ravencrest",
		},
		["profiles"] = {
			["Merii - Ravencrest"] = {
			},
			["Shyanne - Ravencrest"] = {
			},
			["Isrid - Ravencrest"] = {
			},
			["Gwaenni - Ravencrest"] = {
			},
			["Velann - Ravencrest"] = {
			},
			["Mareen - Ravencrest"] = {
			},
		},
	},
}
